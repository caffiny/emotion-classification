import os
import pickle
import numpy as np
import pandas as pd
import tensorflow as tf
import matplotlib.pyplot as plt
from tensorflow.python.keras.models import load_model
from tensorflow.python.keras.preprocessing import image
from keras_preprocessing.image import ImageDataGenerator
from tensorflow.python.keras.layers import Conv2D, MaxPooling2D, Flatten, Dense, Dropout


# todo: 准备数据集
whichDataSet = 'CK'                         # 选择用哪个数据集 CK / fer2013 / jaffe
trainDir = f'./{whichDataSet}/train/'       # 训练集路径
trainingDataGenerator = ImageDataGenerator(
    rescale=1. / 255,           # 值将在执行其他处理前乘到整个图像上
    rotation_range=40,          # 整数，数据提升时图片随机转动的角度
    width_shift_range=0.2,      # 浮点数，图片宽度的某个比例，数据提升时图片随机水平偏移的幅度
    height_shift_range=0.2,     # 浮点数，图片高度的某个比例，数据提升时图片随机竖直偏移的幅度
    shear_range=0.2,            # 浮点数，剪切强度（逆时针方向的剪切变换角度）。是用来进行剪切变换的程度
    zoom_range=0.2,             # 用来进行随机的放大
    validation_split=0.25,
    horizontal_flip=True,       # 布尔值，进行随机水平翻转。随机的对图片进行水平翻转，这个参数适用于水平翻转不影响图片语义的时候
    fill_mode='nearest'         # 'constant','nearest','reflect','wrap'之一，当进行变换时超出边界的点将根据本参数给定的方法进行处理
)
trainGenerator = trainingDataGenerator.flow_from_directory(
    trainDir, subset='training', target_size=(48, 48), class_mode='categorical'
)
validGenerator = trainingDataGenerator.flow_from_directory(
    trainDir, subset='validation', target_size=(48, 48), class_mode='categorical'
)

# todo: 准备标签
class2id = {v: k for k, v in trainGenerator.class_indices.items()}
with open(f'./{whichDataSet}/class2id.pkl', 'wb') as f:
    pickle.dump(class2id, f)
f.close()

# todo: 准备模型
model = tf.keras.models.Sequential([
    # 1
    Conv2D(16, (5, 5), activation='relu', input_shape=(48, 48, 3), padding='same'),
    MaxPooling2D(2, 2),
    # 2
    Conv2D(32, (5, 5), activation='relu', padding='same'),
    MaxPooling2D(2, 2),
    # 3
    Conv2D(32, (5, 5), activation='relu', padding='same'),
    MaxPooling2D(2, 2),
    # 4
    Flatten(),
    Dense(128, activation='relu'),
    Dropout(0.5),
    Dense(7, activation='softmax')
])
model.summary()
model.compile(loss='categorical_crossentropy', optimizer='Adam', metrics=['accuracy'])

# todo: 训练模型
history = model.fit_generator(trainGenerator, epochs=100, validation_data=validGenerator, verbose=1)
model.save(f'./{whichDataSet}/{whichDataSet}.h5')
acc = history.history['acc']
val_acc = history.history['val_acc']
loss = history.history['loss']
val_loss = history.history['val_loss']
epochs = range(len(acc))
plt.plot(epochs, acc, 'r', label='Training accuracy')
plt.plot(epochs, val_acc, 'b', label='validation accuracy')
plt.title('Training and validation accuracy')
plt.legend(loc=0)
plt.show()

# todo: 测试模型
# 准备测试集
model = load_model(f'./{whichDataSet}/{whichDataSet}.h5')
with open(f'./{whichDataSet}/class2id.pkl', 'rb') as f:
    class2id = pickle.load(f)
f.close()
fileList = os.listdir(f'./{whichDataSet}/test/')
testSet = []
for file in fileList:
    img = image.load_img(f'./{whichDataSet}/test/{file}', target_size=(48, 48))
    testSet.append(image.img_to_array(img))
testSet = np.array(testSet)
# 模型预测
modelOutput = model.predict(testSet, batch_size=10)
# 保存结果
getLabel = np.argmax(modelOutput, axis=1).flatten()
df = pd.DataFrame(columns=['file', 'label'])
for i in range(len(getLabel)):
    df.append({
        'file': fileList[i], 'label': class2id[getLabel[i]]
    }, ignore_index=True)
df.to_csv(f'./{whichDataSet}/result.csv')
