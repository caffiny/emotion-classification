# Environment

|       Package       | Version |
| :-----------------: | :-----: |
| Keras-Preprocessing |  1.1.2  |
|        numpy        | 1.20.3  |
|    scikit-image     | 0.18.3  |
|    scikit-learn     |  1.0.1  |
|   tensorflow-gpu    | 1.14.0  |



# Data

代码用到的数据集是自己处理过, 分好训练集和测试集, [下载链接点击跳转](https://download.csdn.net/download/JackyAce6880/85231482)

或者在[我的博客下给我留言](https://blog.csdn.net/JackyAce6880/article/details/124451551), 留下邮箱地址后发给你

将 CK/jaffe/fer2013 三个数据集放到和代码**同一个目录**下



# HOG

打开 **HOG.py**, 直接运行



# LBP

打开 **LBP.py**, 直接运行



# CNN

打开 **CNN.py**, 直接运行



# BLOG

博客地址 -> https://blog.csdn.net/JackyAce6880/article/details/124451551

