import os
import cv2
import seaborn
import sklearn
import numpy as np
import matplotlib.pyplot as plt
from skimage.feature import hog
from sklearn.naive_bayes import GaussianNB
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, precision_score, recall_score, confusion_matrix


def hogSingle(img):
    feature, _ = hog(img, orientations=9, pixels_per_cell=(6, 6), cells_per_block=(6, 6),
                     block_norm='L2-Hys', visualize=True)
    return [feature]


def hogBatch(imgs):
    featureList = []
    for img in imgs:
        feature, _ = hog(img, orientations=9, pixels_per_cell=(6, 6), cells_per_block=(6, 6),
                         block_norm='L2-Hys', visualize=True)
        featureList.append(feature)
    return featureList


def lbpSingle(img):
    h, w = img.shape
    temp_1 = np.zeros(img.shape)
    for i in range(1, h - 1):
        for j in range(1, w - 1):
            temp_2 = (img[i - 1:i + 2, j - 1:j + 2] > img[i][j]).astype(np.int8)
            temp_2 = temp_2.reshape(9)
            temp_2 = np.delete(temp_2, 4)
            temp_2[3], temp_2[4] = temp_2[4], temp_2[3]
            temp_2[5], temp_2[7] = temp_2[7], temp_2[5]
            temp_2 = ''.join('%s' % i for i in temp_2)
            temp_1[i][j] = int(temp_2, 2)
    return [temp_1.flatten()]


def lbpBatch(imgs):
    imgsFeatureList = []
    for img in imgs:
        imgsFeatureList.append(lbpSingle(img)[0])
    return imgsFeatureList


def readData(dataName, label2id):
    X, Y = [], []
    path = f'./{dataName}/train'
    for label in os.listdir(path):
        for image in os.listdir(os.path.join(path, label)):
            img = cv2.imread(os.path.join(path, label, image), cv2.IMREAD_GRAYSCALE)
            img = img / 255.0
            X.append(img)
            Y.append(label2id[label])
    return X, Y


def prepareModel(name):
    if name == 'svm':
        m = sklearn.svm.SVC(C=2, kernel='rbf', gamma=10, decision_function_shape='ovr')  # acc=0.9534
    elif name == 'knn':
        m = KNeighborsClassifier(n_neighbors=1)
    elif name == 'dt':
        m = DecisionTreeClassifier()
    elif name == 'nb':
        m = GaussianNB()
    elif name == 'lg':
        m = LogisticRegression()
    else:
        m = RandomForestClassifier(n_estimators=180, random_state=0)
    return m


def plotConfusionMatrix(data):
    tick = ['angry', 'disgusted', 'fearful', 'happy', 'neutral', 'sad', 'surprised']
    f, ax = plt.subplots(figsize=(7, 5))
    ax.tick_params(axis='y', labelsize=15)
    ax.tick_params(axis='x', labelsize=15)
    seaborn.set(font_scale=1.2)
    plt.rc('font', family='Times New Roman', size=15)
    seaborn.heatmap(data, fmt='g', cmap='Blues', annot=True, cbar=True, xticklabels=tick, yticklabels=tick, ax=ax)
    plt.title('Confusion Matrix', fontsize='x-large')
    plt.show()


def test(dataName, algorithmName, model, id2label):
    path = f'./{dataName}/test'
    for file in os.listdir(path):
        img = cv2.imread(os.path.join(path, file), cv2.IMREAD_GRAYSCALE)
        img = img / 255.0
        if algorithmName == 'hog':
            X_Single = hogSingle(img)
        else:
            X_Single = lbpSingle(img)
        predict = model.predict(X_Single)  # 可以在这里选择分类器的类别
        print(f'{file} -> {id2label[predict.flatten()[0]]}')


def main(dataName, algorithmName):
    label2id = {'angry': 0, 'disgusted': 1, 'fearful': 2, 'happy': 3, 'neutral': 4, 'sad': 5, 'surprised': 6}
    id2label = {v: k for k, v in label2id.items()}

    X, Y = readData(dataName, label2id)
    if algorithmName == 'hog':
        X = hogBatch(X)
    else:
        X = lbpBatch(X)
    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.25, random_state=42)

    model = prepareModel(name='rf')
    model.fit(X_train, Y_train)
    Y_predict = model.predict(X_test)

    acc = accuracy_score(Y_test, Y_predict)
    precision = precision_score(Y_test, Y_predict, average='macro')
    recall = recall_score(Y_test, Y_predict, average='macro')
    cm = confusion_matrix(Y_test, Y_predict)
    print(cm)
    print('Acc: ', acc)
    print('Precision: ', precision)
    print('Recall: ', recall)

    test(dataName, algorithmName, model, id2label)
